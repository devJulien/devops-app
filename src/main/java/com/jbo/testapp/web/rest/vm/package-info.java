/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jbo.testapp.web.rest.vm;
